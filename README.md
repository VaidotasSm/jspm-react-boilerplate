# JSPM React boilerplate

Includes:

* Node - serves static files.
* JSPM
* Gulp
* React
* React Router
* Redux
* redux-api-middleware - for REST API calls.
* Bootswatch


# Technical

## Requirements

* Node v6.0+

## How To

**PROD**

* `npm run init` - download dependencies.
* `npm run build` - build.
* `npm start`
* Open [http://localhost:8080](http://localhost:8080)

**DEV**

* `npm run init` - download dependencies.
* `npm run start:dev`
* Open [http://localhost:8080](http://localhost:8080)


## Manual dependency install commands

* `npm init`
* `npm install express body-parser --save`
* `npm install gulp run-sequence gulp-nodemon gulp-mocha del child_process gulp-html-replace --save-dev`
* `npm install jspm --save-dev`
* `jspm init`
* `jspm install react react-dom jsx react-router`
* `jspm install redux react-redux npm:redux-thunk npm:redux-api-middleware`
* `jspm install npm:bootswatch`