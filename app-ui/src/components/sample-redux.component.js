import React from "react";
import {connect} from "react-redux";
import {SampleActions} from "../redux/actions/app-sample.actions.js";
import {decorateComponent} from "../redux/component-decorator.js";

class SampleReduxComponent extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.onIncreaseCount = this.onIncreaseCount.bind(this);
        this.onGetSampleData = this.onGetSampleData.bind(this);
    }

    onIncreaseCount() {
        this.props.dispatch(SampleActions.increaseCount(1));
    }

    onGetSampleData() {
        this.props.dispatch(SampleActions.getSampleData());
    }

    render() {
        return (
            <div>
                <h3>Sample Redux component</h3>
                <div>
                    <button className="btn btn-default" onClick={this.onIncreaseCount}>Increase count</button>
                    <div>State: {this.props.reducer.count}</div>
                </div>
                <div>
                    <button className="btn btn-default" onClick={this.onGetSampleData}>Fetch sample data</button>
                    <div>Result: {this.props.reducer.sampleData && this.props.reducer.sampleData.name}</div>
                </div>
            </div>
        );
    }
}

const decoratedComponent = decorateComponent(SampleReduxComponent, 'sampleReducer');
export {decoratedComponent as SampleReduxComponent};
