import {combineReducers} from "redux";
import {sampleReducer} from "./reducers/app-sample.reducer.js";

const rootReducer = combineReducers({
    sampleReducer: sampleReducer
});
export {rootReducer};

