import {Actions} from "../actions/actions.js";

function sampleReducer(state = {count: 0}, action) {
    console.log(action);
    switch (action.type) {
        case Actions.GLOBAL_COUNT_INCREASED:
            return Object.assign({}, state, {count: state.count + action.payload});
        case Actions.SAMPLE_DATA_LOAD_SUCCESS:
            return Object.assign({}, state, {sampleData: action.payload});
        case Actions.SAMPLE_DATA_LOAD_FAILURE:
            return Object.assign({}, state, {sampleDataErr: action.payload});
        default:
            return state;
    }
}
export {sampleReducer};

