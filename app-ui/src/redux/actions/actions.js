const Actions = {
    GLOBAL_COUNT_INCREASED: 'GLOBAL_COUNT_INCREASED',
    SAMPLE_DATA_LOAD_STARTED: 'SAMPLE_DATA_LOAD_STARTED',
    SAMPLE_DATA_LOAD_SUCCESS: 'SAMPLE_DATA_LOAD_SUCCESS',
    SAMPLE_DATA_LOAD_FAILURE: 'SAMPLE_DATA_LOAD_FAILURE'
};

export {Actions};
