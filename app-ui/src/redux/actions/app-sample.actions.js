import {Actions} from "./actions.js";
import {CALL_API} from 'redux-api-middleware';

export class SampleActions {

    static increaseCount(increaseBy) {
        return {type: Actions.GLOBAL_COUNT_INCREASED, payload: increaseBy};
    }

    static getSampleData() {
        return {
            [CALL_API]: {
                endpoint: '/api/sample',
                method: 'GET',
                types: [
                    Actions.SAMPLE_DATA_LOAD_STARTED,
                    Actions.SAMPLE_DATA_LOAD_SUCCESS,
                    Actions.SAMPLE_DATA_LOAD_FAILURE
                ]
            }
        };
    }
}
