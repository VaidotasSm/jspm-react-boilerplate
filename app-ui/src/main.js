import React from "react";
import ReactDom from "react-dom";
import {Router, Route, IndexRoute, hashHistory} from "react-router";
import {Provider} from "react-redux";
import {configureStore} from "./redux/redux-store";
import {SampleReduxComponent} from "./components/sample-redux.component.js";

const MainPage = function (props) {
    return (
        <div>
            <div>Header</div>
            <div>{props.children}</div>
        </div>
    );
};
const NotFoundPage = function (props) {
    return (<div>Page not found</div>);
};

const store = configureStore();
ReactDom.render(
    <Provider store={store}>
        <Router history={hashHistory}>
            <Route path="/" component={MainPage}>
                <IndexRoute component={SampleReduxComponent}/>
                <Route path="*" component={NotFoundPage}/>
            </Route>
        </Router>
    </Provider>,
    document.getElementById('app')
);
