const express = require('express');
const path = require("path");
const app = express();

const profile = process.env.PROFILE || 'prod';
const port = process.env.PORT || 8080;

if (profile === 'dev') {
    app.use('/app', express.static(path.join(__dirname, '../..', 'app-ui')));
} else {
    app.use('/app', express.static(path.join(__dirname, '../..', 'app-ui/dest')));
}

app.get('/api/sample', (req, res) => {
    console.log('/api/sample');
    res.status(200).json({name: 'Name1', email: 'email1@email.com'});
});

app.get('/', (req, res) => res.redirect('/app/index.html'));

app.listen(port, () => {
    console.log(`Started as "${profile}" profile, on port ${port}`);
});
